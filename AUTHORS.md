# Authors

* Andrey Vasilyev
  * Developer, 2023
* Vladislav Larionov
  * Developer, 2023
  * Reviewer, 2023
* Denis Grigorev
  * Developer, 2023
* Evgeniy Samohin, <e.samohin@omp.ru>
  * Reviewer, 2023
  * Maintainer, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer, 2023
* Oleg Shevchenko
  * Reviewer, 2023
