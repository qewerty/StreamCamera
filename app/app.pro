# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = app

TARGET = ru.auroraos.StreamCamera

QT += core multimedia quick

CONFIG += \
    auroraapp \
    c++17 \

PKGCONFIG += \
    streamcamera \
    streamcamera-qt5 \

SOURCES += \
    src/cameraconfig.cpp \
    src/camerastream.cpp \
    src/encodedframe.cpp \
    src/encodedframeprovider.cpp \
    src/fileopener.cpp \
    src/frameprovider.cpp \
    src/main.cpp \
    src/mp4file.cpp \
    src/ycbcrtorgbimageconverter.cpp

HEADERS +=  \
    src/cameraconfig.h \
    src/camerastream.h \
    src/encodedframe.h \
    src/encodedframeprovider.h \
    src/fileopener.h \
    src/frameprovider.h \
    src/mp4file.h \
    src/ycbcrframemetatypeconnector.h \
    src/ycbcrtorgbimageconverter.h

DISTFILES += \
    ru.auroraos.StreamCamera.desktop

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n

TRANSLATIONS += \
    translations/ru.auroraos.StreamCamera.ts \
    translations/ru.auroraos.StreamCamera-ru.ts \

RESOURCES += \
    resources.qrc \

INCLUDEPATH += \
    $$PWD/../libs/ffmpeg/include \

DEPENDPATH += \
    $$PWD/../libs/ffmpeg/include \

GCC_INFO = $$system($$QMAKE_CXX -dumpmachine)
contains(GCC_INFO, .*aarch64.*) {
    FFMPEG_LIB_FOLDER = aarch64
} else {
    contains(GCC_INFO, .*armv7hl.*) {
        FFMPEG_LIB_FOLDER = arm7hl
    } else {
        contains(GCC_INFO, .*i486.*) {
            FFMPEG_LIB_FOLDER = i486
        } else {
            FFMPEG_LIB_FOLDER = x86_64
        }
    }
}

ffmpeg_install.path = /usr/share/ru.auroraos.StreamCamera/lib
ffmpeg_install.files = $$PWD/../libs/ffmpeg/lib-$$FFMPEG_LIB_FOLDER/lib*

ffmpeg_install.CONFIG = no_check_exist

QMAKE_EXTRA_TARGETS +=\
    ffmpeg_install

INSTALLS += \
    ffmpeg_install

LIBS += -L$$PWD/../libs/ffmpeg/lib-$$FFMPEG_LIB_FOLDER \
        -lavformat \
        -lavcodec \
        -lswscale \
        -lavutil \
        -lavfilter \
        -lswresample \
        -lavdevice \
        -lz
