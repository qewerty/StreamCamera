// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import QtMultimedia 5.6
import Sailfish.Silica 1.0
import ru.auroraos.StreamCamera 1.0

Page {
    objectName: "mainPage"
    allowedOrientations: Orientation.Portrait
    Component.onCompleted: {
        cameraStream.start();
    }
    Item {
        id: pageRoot
        anchors.fill: parent

        VideoOutput {
            id: videoOutput
            anchors.fill: parent
            source: cameraStream.viewfinder
            orientation: cameraStream.orientation
            fillMode: VideoOutput.PreserveAspectFit
        }

        StreamCamera {
            id: cameraStream
            objectName: "CameraStream"

            onTimeUpdated: {
                timePassed.text = time;
            }
            onCapturingStateUpdated: {
                if (started) {
                    shootButton.source = "qrc:/icons/stop.png";
                    showResDirBtn.visible = false;
                } else {
                    shootButton.source = "qrc:/icons/rec.png";
                    showResDirBtn.visible = true;
                }
            }
            onCameraError: {
                cameraStream.visible = false;
                timePassed.visible = false;
                showResDirBtn.visible = false;
                shootButton.visible = false;
                errorMsg.visible = true;
                errorMsg.text = msg;
            }
        }

        Text {
            id: errorMsg
            text: "00:00:00"
            anchors {
                fill: pageRoot
                margins: Theme.horizontalPageMargin
            }
            color: "black"
            visible: false
        }
        Text {
            id: timePassed
            text: "00:00:00"
            anchors {
                top: pageRoot.top
                horizontalCenter: pageRoot.horizontalCenter
                margins: Theme.horizontalPageMargin
            }
            color: "white"
        }
        IconButton {
            objectName: "aboutButton"
            icon.source: "image://theme/icon-m-about?" + (Theme.lightPrimaryColor)
            anchors {
                top: parent.top
                right: parent.right
                margins: Theme.horizontalPageMargin
            }
            onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
        }
        Image {
            id: shootButton
            objectName: "shootButton"
            source: "qrc:/icons/rec.png"
            width: Theme.iconSizeLarge
            height: Theme.iconSizeLarge
            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                margins: Theme.horizontalPageMargin
            }
            MouseArea {
                id: imageRecArea
                anchors.fill: parent
                onClicked: {
                    cameraStream.startCapturing();
                }
            }
        }
        Image {
            id: showResDirBtn
            anchors {
                right: parent.right
                bottom: shootButton.bottom
                rightMargin: 2 * Theme.horizontalPageMargin
            }
            width: Theme.iconSizeLarge
            height: Theme.iconSizeLarge
            source: "image://theme/icon-m-folder?#FFFFFF"
            MouseArea {
                id: showResDirBtnArea
                anchors.fill: parent
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("ResultDirectoryPage.qml"));
                }
            }
        }
    }
}
