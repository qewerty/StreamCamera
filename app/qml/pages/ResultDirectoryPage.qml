// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
import QtQuick 2.0
import Sailfish.Silica 1.0
import Qt.labs.folderlistmodel 2.1
import ru.auroraos.FileOpener 1.0

Page {
    property string currentFolder: StandardPaths.home

    objectName: "resultPage"
    allowedOrientations: Orientation.All

    FolderListModel {
        id: folderModel
        folder: currentFolder + '/Videos/StreamCamera'
        nameFilters: ["*.mp4", "*.h264", "*.264"]
    }

    FileOpener {
        id: fileOpener
    }

    SilicaListView {
        anchors {
            fill: parent
        }
        model: folderModel
        header: PageHeader {
            title: qsTr("StreamCamera")
        }
        delegate: ListItem {
            id: listItem
            width: parent.width

            Image {
                id: fileIcon
                anchors {
                    left: parent.left
                    verticalCenter: parent.verticalCenter
                    leftMargin: Theme.horizontalPageMargin
                }
                source: "image://theme/icon-m-video?" + (pressed ? Theme.highlightColor : Theme.primaryColor)
            }

            Label {
                anchors {
                    left: fileIcon.right
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                    margins: Theme.horizontalPageMargin
                }
                text: fileName
                color: pressed ? Theme.highlightColor : Theme.primaryColor
            }
            onClicked: {
                fileOpener.openFile(folderModel.folder + "/" + fileName);
            }
        }
        VerticalScrollDecorator {
        }
    }
}
