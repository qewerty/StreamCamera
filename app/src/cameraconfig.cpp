// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "cameraconfig.h"

CameraConfig::CameraConfig() : QObject(), m_manager(StreamCameraManager()), m_camera(nullptr) { }

bool CameraConfig::init(bool skipMaxResolution)
{
    if (m_manager->getNumberOfCameras()) {
        if (m_manager->getCameraInfo(0, m_cameraInfo)) {
            std::vector<Aurora::StreamCamera::CameraCapability> caps;
            if (m_manager->queryCapabilities(m_cameraInfo.id, caps)) {
                if (caps.at(0).width < 300) {
                    qDebug() << "Camera capability is reversed";
                    std::reverse(caps.begin(), caps.end());
                }
                if (skipMaxResolution) {
                    qDebug() << "The maximum resolution is skipped";
                    caps.erase(caps.begin());
                }
                for (const Aurora::StreamCamera::CameraCapability &cap : caps) {
                    if (EncodedFrameProvider::isEncoderAvalibleForCapability(cap)) {
                        m_camera = m_manager->openCamera(m_cameraInfo.id);
                        m_cameraCapability = cap;
                        qDebug() << "    "
                                 << "Camera capability" << cap.width << "x" << cap.height << ":"
                                 << cap.fps << "fps is selected for the encoder";
                        emit orientationChanged(orientation());
                        return true;
                    } else {
                        qDebug() << "    "
                                 << "Camera capability" << cap.width << "x" << cap.height << ":"
                                 << cap.fps << "fps is not avaliable for the encoder";
                    }
                }
            }
            if (m_camera == nullptr) {
                qDebug() << "There is no avaliable camera capability";
                emit cameraInitError(tr("There is no avaliable camera capability"));
                return false;
            }
        } else {
            qDebug() << "Error during camera info getting";
            emit cameraInitError(tr("Error during camera info getting"));
            return false;
        }
    } else {
        qDebug() << "There is no avaliable camera";
        emit cameraInitError(tr("There is no avaliable camera"));
        return false;
    }
    return false;
}

std::shared_ptr<Aurora::StreamCamera::Camera> CameraConfig::getCamera()
{
    return m_camera;
}

Aurora::StreamCamera::CameraCapability CameraConfig::getCameraCapability()
{
    return m_cameraCapability;
}

int CameraConfig::orientation() const
{
    if (m_camera) {
        return facingFront() ? mountAngle() : 360 - mountAngle();
    }
    return 0;
}

int CameraConfig::mountAngle() const
{
    return m_cameraInfo.mountAngle;
}

bool CameraConfig::facingFront() const
{
    return m_cameraInfo.facing == Aurora::StreamCamera::CameraFacing::Front;
}
