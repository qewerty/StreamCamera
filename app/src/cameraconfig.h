// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CAMERACONFIG_H
#define CAMERACONFIG_H

#include <QDebug>
#include <QObject>
#include <streamcamera/streamcamera.h>
#include <streamcamera/streamcamera-codec.h>
#include "encodedframeprovider.h"

class CameraConfig : public QObject
{
    Q_OBJECT
public:
    CameraConfig();
    bool init(bool skipMaxResolution = true);
    std::shared_ptr<Aurora::StreamCamera::Camera> getCamera();
    Aurora::StreamCamera::CameraCapability getCameraCapability();
    int orientation() const;
    int mountAngle() const;
    bool facingFront() const;

private:
    Aurora::StreamCamera::CameraManager *m_manager;
    std::shared_ptr<Aurora::StreamCamera::Camera> m_camera;
    Aurora::StreamCamera::CameraCapability m_cameraCapability;
    Aurora::StreamCamera::CameraInfo m_cameraInfo;

signals:
    void cameraInitError(const QString &msg);
    void orientationChanged(int orientation);
};

#endif // CAMERACONFIG_H
