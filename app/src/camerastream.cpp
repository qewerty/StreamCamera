// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "camerastream.h"
#include <QDebug>
#include <QTime>
#include <QSGSimpleTextureNode>

#include "frameprovider.h"

CameraStream::CameraStream(QQuickItem *parent) : QQuickItem(parent), m_capturingStarted(false)
{
    m_cameraConfig = new CameraConfig();
    connect(m_cameraConfig, &CameraConfig::cameraInitError, this, &CameraStream::cameraError);
    connect(m_cameraConfig, &CameraConfig::orientationChanged, this,
            &CameraStream::orientationChanged);
    setFlag(ItemHasContents, true);
    connect(&m_timer, &QTimer::timeout, this, &CameraStream::updateTime);
}

void CameraStream::start()
{
    if (m_cameraConfig->init()) {
        m_frameListener = new FrameProvider(m_cameraConfig, &m_viewfinder);
        connect(m_frameListener, &FrameProvider::cameraError, this, &CameraStream::cameraError);
        m_frameListener->start();
    }
}

QObject *CameraStream::viewfinder()
{
    return &m_viewfinder;
}

int CameraStream::orientation() const
{
    return m_cameraConfig->orientation();
}

/**
 * Frees the main camera components.
 * If the frame provider is running then stops is at first and then frees.
 */
CameraStream::~CameraStream()
{
    m_capturingStarted = false;
    m_timer.stop();
    m_timeCounter.invalidate();
    if (m_frameListener) {
        delete m_frameListener;
        m_frameListener = nullptr;
    }
    if (m_cameraConfig) {
        delete m_cameraConfig;
        m_cameraConfig = nullptr;
    }
}

void CameraStream::startCapturing()
{
    if (m_capturingStarted) {
        m_frameListener->stopCapturing();
        qDebug() << "The video recorded with the duration of"
                 << QTime(0, 0).addMSecs(m_timeCounter.elapsed()).toString("hh:mm:ss");
        emit capturingStateUpdated(false);
        m_capturingStarted = false;
        m_timer.stop();
        m_timeCounter.invalidate();
        emit timeUpdated(QTime(0, 0).toString("hh:mm:ss"));
    } else {
        m_capturingStarted = true;
        m_frameListener->startCapturing();
        m_timeCounter.restart();
        m_timer.start(1000);
        emit capturingStateUpdated(true);
    }
}

void CameraStream::updateTime()
{
    QString timeStr = QTime(0, 0).addMSecs(m_timeCounter.elapsed()).toString("hh:mm:ss");
    emit timeUpdated(timeStr);
}
