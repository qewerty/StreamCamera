// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CAMERASTREAM_H
#define CAMERASTREAM_H

#include <QElapsedTimer>
#include <QTimer>
#include <QQuickItem>
#include <QSGGeometryNode>
#include <QQuickWindow>
#include <streamcamera/videooutput_qt5.h>
#include "cameraconfig.h"

class FrameProvider;

class CameraStream : public QQuickItem
{
    Q_OBJECT

    Q_PROPERTY(QObject *viewfinder READ viewfinder CONSTANT)
    Q_PROPERTY(int orientation READ orientation NOTIFY orientationChanged)

public:
    explicit CameraStream(QQuickItem *parent = nullptr);
    ~CameraStream();
    Q_INVOKABLE void startCapturing();
    Q_INVOKABLE void start();
    QObject *viewfinder();
    int orientation() const;

private:
    bool m_capturingStarted;
    FrameProvider *m_frameListener;
    QTimer m_timer;
    QElapsedTimer m_timeCounter;
    CameraConfig *m_cameraConfig;
    Aurora::StreamCamera::VideoOutputQt5 m_viewfinder;
private slots:
    void updateTime();
signals:
    void timeUpdated(const QString &time);
    void capturingStateUpdated(bool started);
    void cameraError(const QString &msg);
    void orientationChanged(int orientation);
};

#endif // CAMERASTREAM_H
