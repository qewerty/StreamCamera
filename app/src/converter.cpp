// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#include "converter.h"
#include <QSize>

Converter::Converter(QObject *parent) : QObject(parent), QRunnable()
{
    setAutoDelete(true);
    m_runnning = true;
    canConvert = true;
}

void Converter::onConvertNext(int width, int height, uint8_t *y, uint8_t *cr,
                              unsigned short cStride, unsigned short yStride,
                              unsigned short chromaStep)
{
    if (canConvert) {
        m_y = (uint8_t *)malloc(width * height);
        memcpy(m_y, y, width * height);
        int len = strlen((char *)cr);
        m_cr = (uint8_t *)malloc(len);
        memcpy(m_cr, cr, len);
        this->cStride = cStride;
        this->yStride = yStride;
        this->chromaStep = chromaStep;
        this->m_width = width;
        this->m_height = height;
        canConvert = false;
    }
    free(y);
    free(cr);
}
void Converter::run()
{
    while (m_runnning) {
        if (!canConvert) {
            QSize size(m_width, m_height);
            QImage *image = new QImage(size, QImage::Format_ARGB32);
            // convertNv12ToArgb
            planarYuv420ToArgb(m_y, m_cr + 1, m_cr, yStride, cStride, cStride, chromaStep,
                               reinterpret_cast<quint32 *>(image->bits()), m_width, m_height);
            emit imageConverted(image);
            free(m_y);
            free(m_cr);
            canConvert = true;
        }
    }
}

quint32 Converter::yuvToArgb(qint32 y, qint32 rv, qint32 guv, qint32 bu, qint32 a)
{
    qint32 yy = (y - 16) * 298;

    return (a << 24) | qBound(0, (yy + rv) >> 8, 255) << 16 | qBound(0, (yy - guv) >> 8, 255) << 8
            | qBound(0, (yy + bu) >> 8, 255);
}

void Converter::planarYuv420ToArgb(const uchar *y, const uchar *u, const uchar *v, qint32 yStride,
                                   qint32 uStride, qint32 vStride, qint32 uvPixelStride,
                                   quint32 *rgb, qint32 width, qint32 height)
{
    quint32 *rgb0 = rgb;
    quint32 *rgb1 = rgb + width;

    for (qint32 j = 0; j < height; j += 2) {
        const uchar *lineY0 = y;
        const uchar *lineY1 = y + yStride;
        const uchar *lineU = u;
        const uchar *lineV = v;

        for (qint32 i = 0; i < width; i += 2) {
            const qint32 uu = *lineU - 128;
            const qint32 vv = *lineV - 128;
            const qint32 rv = 409 * vv + 128;
            const qint32 guv = 100 * uu + 208 * vv + 128;
            const qint32 bu = 516 * uu + 128;

            lineU += uvPixelStride;
            lineV += uvPixelStride;
            *rgb0++ = yuvToArgb(*lineY0++, rv, guv, bu);
            *rgb0++ = yuvToArgb(*lineY0++, rv, guv, bu);
            *rgb1++ = yuvToArgb(*lineY1++, rv, guv, bu);
            *rgb1++ = yuvToArgb(*lineY1++, rv, guv, bu);
        }

        y += yStride << 1;
        u += uStride;
        v += vStride;
        rgb0 += width;
        rgb1 += width;
    }
}
