// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause
#ifndef CONVERTER_H
#define CONVERTER_H

#include <QObject>
#include <QRunnable>
#include <QImage>
#include <QDebug>

class Converter : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit Converter(QObject *parent = nullptr);
    void run();
signals:
    void imageConverted(const QImage *image);
public slots:
    void onConvertNext(int width, int height, uint8_t *y, uint8_t *cr, unsigned short cStride,
                       unsigned short yStride, unsigned short chromaStep);

private:
    bool m_runnning;
    bool canConvert;
    int m_width;
    int m_height;
    uint16_t cStride;
    uint16_t yStride;
    uint16_t chromaStep;
    uint8_t *m_y;
    uint8_t *m_cb;
    uint8_t *m_cr;
    quint32 yuvToArgb(qint32 y, qint32 rv, qint32 guv, qint32 bu, qint32 a = 255);
    void planarYuv420ToArgb(const uchar *y, const uchar *u, const uchar *v, qint32 yStride,
                            qint32 uStride, qint32 vStride, qint32 uvPixelStride, quint32 *rgb,
                            qint32 width, qint32 height);
};

#endif // CONVERTER_H
