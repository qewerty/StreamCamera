// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "encodedframe.h"

EncodedFrame::EncodedFrame(uint8_t *encoded, size_t size, uint64_t timestampUs,
                           Aurora::StreamCamera::FrameType type)
    : m_size(size), m_timestampUs(timestampUs), m_type(type)
{
    m_data = static_cast<uint8_t *>(malloc(size));
    memcpy(m_data, encoded, size);
    splitIntoH264NalUnits(m_data, this->m_size);
}

void EncodedFrame::release(void *data)
{
    EncodedFrame *frame = static_cast<EncodedFrame *>(data);
    delete frame;
}

EncodedFrame::~EncodedFrame()
{
    free(m_data);
    for (int i = 0; i < m_naluCount; i++)
        delete m_nalus[i];
}

void EncodedFrame::splitIntoH264NalUnits(const uint8_t *buf, size_t bufSize)
{
    int cur = 0;
    NaluData *nalu = nullptr;
    while (cur + 4 < bufSize) {
        if (buf[cur] == 0 && buf[cur + 1] == 0 && buf[cur + 2] == 0 && buf[cur + 3] == 1) {
            if (nalu == nullptr) {
                nalu = new NaluData();
                nalu->start = cur;
            } else {
                nalu->end = cur;
                nalu->length = nalu->end - nalu->start;
                m_nalus[m_naluCount++] = nalu;
                nalu = new NaluData();
                nalu->start = cur;
            }
        }
        cur += 1;
    }
    if (nalu != nullptr) {
        nalu->end = bufSize;
        nalu->length = nalu->end - nalu->start;
        m_nalus[m_naluCount++] = nalu;
    }
}
