// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef ENCODEDFRAME_H
#define ENCODEDFRAME_H

#include <streamcamera/streamcamera-codec.h>
#include <cstring>

struct NaluData
{
    int start;
    int end;
    int length;
};

class EncodedFrame
{
public:
    EncodedFrame(uint8_t *encoded, size_t size, uint64_t timestampUs,
                 Aurora::StreamCamera::FrameType type);
    ~EncodedFrame();
    static void release(void *data);

    uint8_t *m_data;
    size_t m_size;
    uint64_t m_timestampUs;
    Aurora::StreamCamera::FrameType m_type;
    NaluData *m_nalus[10];
    int m_naluCount = 0;
    static const int STRAT_CODE_LENGTH_IN_BYTES = 4;

private:
    void splitIntoH264NalUnits(const uint8_t *buf, size_t bufSize);
};

#endif // ENCODEDFRAME_H
