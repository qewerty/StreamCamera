// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "encodedframeprovider.h"

EncodedFrameProvider::EncodedFrameProvider(const Config &config)
    : QObject(), StreamCamera::VideoEncoderListener()
{
    m_videoEncoder = createEncoder(config);
    if (m_videoEncoder == nullptr) {
        qDebug() << "Video encoder is invalid";
    }
    m_videoEncoder->setListener(this);
    m_mp4File = new Mp4File(config.width, config.height);
}

bool EncodedFrameProvider::isEncoderAvalibleForCapability(
        const StreamCamera::CameraCapability &cameraCapability)
{
    Config cfg{ cameraCapability.width, cameraCapability.height, cameraCapability.fps, 0, false };
    std::shared_ptr<StreamCamera::VideoEncoder> encoder = createEncoder(cfg);
    if (encoder != nullptr) {
        encoder = nullptr;
        return true;
    }
    return false;
}

std::shared_ptr<StreamCamera::VideoEncoder> EncodedFrameProvider::createEncoder(const Config &cfg)
{
    StreamCamera::CodecManager *m_codecManager = StreamCameraCodecManager();
    if (!m_codecManager->videoEncoderAvailable(Aurora::StreamCamera::CodecType::H264)) {
        qDebug() << "Video encoder for the codec H264 isn't available!";
        return nullptr;
    }
    std::shared_ptr<StreamCamera::VideoEncoder> videoEncoder =
            m_codecManager->createVideoEncoder(StreamCamera::CodecType::H264);

    if (videoEncoder == nullptr) {
        qDebug() << "Video encoder is invalid";
        return nullptr;
    }
    StreamCamera::VideoEncoderMetadata meta;
    meta.codecType = StreamCamera::CodecType::H264;
    meta.width = cfg.width;
    meta.height = cfg.height;
    meta.stride = cfg.width;
    meta.sliceHeight = cfg.height;
    meta.bitrate = 2000000;
    meta.framerate = cfg.fps;

    std::string mirror = cfg.facingFront ? "true" : "false";
    std::string rotation = std::to_string(cfg.mountAngle);

    // Preprocess parameters must be set before codec's init().
    videoEncoder->setParameter(StreamCamera::VideoEncoderParameter::PreprocessRotation, rotation);
    videoEncoder->setParameter(StreamCamera::VideoEncoderParameter::PreprocessMirrorV, mirror);

    if (videoEncoder->init(meta)) {
        qDebug() << "Video encoder initialized";
        return videoEncoder;
    }
    qDebug() << "Video encoder initialization failed";
    return nullptr;
}

void EncodedFrameProvider::encode(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame,
                                  bool forceSync)
{
    if (m_videoEncoder != nullptr && !stopped) {
        m_videoEncoder->encode(frame, forceSync);
    }
}

EncodedFrameProvider::~EncodedFrameProvider()
{
    delete m_mp4File;
    m_videoEncoder = nullptr;
}

void EncodedFrameProvider::onEncodedFrame(uint8_t *data, size_t size, uint64_t timestampUs,
                                          StreamCamera::FrameType type)
{
    EncodedFrame frame(data, size, timestampUs, type);
    if (m_videoEncoder != nullptr && !stopped)
        m_mp4File->addEncodedFrame(&frame, type == StreamCamera::FrameType::Key);
}

void EncodedFrameProvider::onEncoderError(const std::string &errorDescription)
{
    qDebug() << "Video encoder error: " << QString::fromStdString(errorDescription);
}

void EncodedFrameProvider::onEncoderParameterChanged(StreamCamera::VideoEncoderParameter param,
                                                     const std::string &value)
{
    qDebug() << "Video encoder parameter changed: " << static_cast<unsigned int>(param) << " = "
             << QString::fromStdString(value);
}

void EncodedFrameProvider::onEncoderEOS() { }

void EncodedFrameProvider::stop()
{
    stopped = true;
    m_mp4File->saveAndClose();
}
