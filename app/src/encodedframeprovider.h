// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef ENCODEDFRAMEPROVIDER_H
#define ENCODEDFRAMEPROVIDER_H

#include "mp4file.h"
#include <streamcamera/streamcamera-codec.h>
#include <QObject>
#include <QDebug>

using namespace Aurora;

class EncodedFrameProvider : public QObject, public StreamCamera::VideoEncoderListener
{
    Q_OBJECT
public:
    struct Config
    {
        unsigned int width;
        unsigned int height;
        unsigned int fps;
        int mountAngle;
        bool facingFront;
    };

    EncodedFrameProvider(const Config &config);
    ~EncodedFrameProvider();
    static bool
    isEncoderAvalibleForCapability(const StreamCamera::CameraCapability &cameraCapability);

public slots:
    void stop();
    void encode(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame, bool forceSync);

private:
    std::shared_ptr<StreamCamera::VideoEncoder> m_videoEncoder;
    Mp4File *m_mp4File;
    bool stopped = false;
    static std::shared_ptr<StreamCamera::VideoEncoder> createEncoder(const Config &config);
    void onEncodedFrame(uint8_t *data, size_t size, uint64_t timestampUs,
                        StreamCamera::FrameType type);
    void onEncoderError(const std::string &errorDescription);
    void onEncoderParameterChanged(StreamCamera::VideoEncoderParameter param,
                                   const std::string &value);
    void onEncoderEOS();
};

#endif // ENCODEDFRAMEPROVIDER_H
