// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "fileopener.h"
#include <QDebug>
#include <QDesktopServices>
#include <QUrl>

FileOpener::FileOpener(QObject *parent) : QObject(parent) { }

void FileOpener::openFile(const QString &path)
{
    QDesktopServices::openUrl(QUrl(path));
}
