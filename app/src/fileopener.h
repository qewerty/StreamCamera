// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef FILEOPENER_H
#define FILEOPENER_H

#include <QObject>

class FileOpener : public QObject
{
    Q_OBJECT
public:
    explicit FileOpener(QObject *parent = nullptr);
    Q_INVOKABLE void openFile(const QString &path);

signals:
};

#endif // FILEOPENER_H
