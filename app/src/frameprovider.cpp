// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "frameprovider.h"
#include <QDebug>
#include <QGuiApplication>
#include <QThreadPool>
#include "encodedframe.h"

FrameProvider::FrameProvider(CameraConfig *cameraConfig,
                             Aurora::StreamCamera::VideoOutputQt5 *viewfinder)
    : QObject(),
      Aurora::StreamCamera::CameraListener(),
      m_encodedFrameListener(nullptr),
      m_viewfinder(viewfinder),
      m_capturingEnabled(false),
      m_cameraStarted(false)
{
    m_camera = cameraConfig->getCamera();
    m_cameraCapability = cameraConfig->getCameraCapability();
    m_encoderConfig.width = m_cameraCapability.width;
    m_encoderConfig.height = m_cameraCapability.height;
    m_encoderConfig.fps = m_cameraCapability.fps;
    m_encoderConfig.mountAngle = cameraConfig->mountAngle();
    m_encoderConfig.facingFront = cameraConfig->facingFront();
    m_camera->setListener(this);
}

/**
 * Stops the camera and the frame encoder.
 */
void FrameProvider::stop()
{
    stopCapturing();
    m_camera->stopCapture();
    m_cameraStarted = false;
    qDebug() << "Camera stopped";
}

/**
 * Stops the camera and frees the encoder.
 * If the encoder is running then it will be stopped at first and then freed.
 */
FrameProvider::~FrameProvider()
{
    stop();
    if (m_encodedFrameListener) {
        delete m_encodedFrameListener;
        m_encodedFrameListener = nullptr;
    }
}

void FrameProvider::startCapturing()
{
    qDebug() << "Starting capturing";
    start();
    m_frameNumber = 0;
    if (m_encodedFrameListener) {
        delete m_encodedFrameListener;
        m_encodedFrameListener = nullptr;
    }
    m_encodedFrameListener = new EncodedFrameProvider(m_encoderConfig);
    connect(this, &FrameProvider::stopRecord, m_encodedFrameListener, &EncodedFrameProvider::stop);
    this->m_capturingEnabled = true;
    qDebug() << "Capturing started";
}

void FrameProvider::stopCapturing()
{
    if (this->m_capturingEnabled) {
        qDebug() << "Stopping capturing";
        this->m_capturingEnabled = false;
        emit stopRecord();
        qDebug() << "Frames sent to save:" << m_frameNumber;
        qDebug() << "Capturing stopped";
    }
}

bool FrameProvider::isCapuringEnabled()
{
    return m_capturingEnabled;
}

void FrameProvider::onCameraFrame(std::shared_ptr<Aurora::StreamCamera::GraphicBuffer> buffer)
{
    std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame = buffer->mapYCbCr();
    if (m_capturingEnabled) {
        bool needKeyFrame = !(m_frameNumber++ % m_cameraCapability.fps);
        m_encodedFrameListener->encode(frame, needKeyFrame);
    }
    if (QGuiApplication::focusWindow() && m_viewfinder) {
        m_viewfinder->onGraphicBuffer(buffer);
    }
}

void FrameProvider::onCameraParameterChanged(Aurora::StreamCamera::CameraParameter param,
                                             const std::string &value)
{
    qDebug() << "Camera parameter changed:" << static_cast<unsigned int>(param) << " = "
             << QString::fromStdString(value);
}

void FrameProvider::onCameraError(const std::string &errorDescription)
{
    qDebug() << "Camera error:" << QString::fromStdString(errorDescription);
    emit cameraError("Camera error:" + QString::fromStdString(errorDescription));
}

void FrameProvider::start()
{
    if (m_camera && !m_cameraStarted) {
        m_camera->startCapture(m_cameraCapability);
        m_cameraStarted = true;
        qDebug() << "Camera started";
    } else {
        qDebug() << "Camera is not avaliable";
    }
}
