// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef FRAMEPROVIDER_H
#define FRAMEPROVIDER_H

#include "encodedframeprovider.h"
#include <streamcamera/streamcamera.h>
#include <streamcamera/streamcamera-codec.h>
#include <streamcamera/videooutput_qt5.h>
#include "ycbcrtorgbimageconverter.h"
#include <QObject>
#include "cameraconfig.h"

class FrameProvider : public QObject, public Aurora::StreamCamera::CameraListener
{
    Q_OBJECT
public:
    FrameProvider(CameraConfig *cameraConfig, Aurora::StreamCamera::VideoOutputQt5 *viewfinder);
    ~FrameProvider();
    void start();
    void stop();
    void setCapuringEnabled(bool capuringEnabled);
    void startCapturing();
    void stopCapturing();
    bool isCapuringEnabled();

signals:
    void convertImageToARGB(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame);
    void stopRecord();
    void cameraError(const QString &msg);

private:
    void onCameraFrame(std::shared_ptr<Aurora::StreamCamera::GraphicBuffer> buffer);
    void onCameraError(const std::string &errorDescription);
    void onCameraParameterChanged(Aurora::StreamCamera::CameraParameter param,
                                  const std::string &value);
    EncodedFrameProvider *m_encodedFrameListener;
    std::shared_ptr<Aurora::StreamCamera::Camera> m_camera;
    EncodedFrameProvider::Config m_encoderConfig;
    Aurora::StreamCamera::CameraCapability m_cameraCapability;
    Aurora::StreamCamera::VideoOutputQt5 *m_viewfinder;
    bool m_capturingEnabled;
    bool m_cameraStarted;
    bool m_encoderAvailable;
    int m_frameNumber = 0;
    YCbCrToRgbImageConverter *m_yCbCrToRgbConverter;
};

#endif // FRAMEPROVIDER_H
