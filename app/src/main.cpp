// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <auroraapp/auroraapp.h>
#include "camerastream.h"
#include "fileopener.h"
#include "ycbcrframemetatypeconnector.h"
#include <QtQuick>
#include <QMetaType>

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("StreamCamera"));
    qmlRegisterType<CameraStream>("ru.auroraos.StreamCamera", 1, 0, "StreamCamera");
    qmlRegisterType<FileOpener>("ru.auroraos.FileOpener", 1, 0, "FileOpener");
    qRegisterMetaType<std::shared_ptr<Aurora::StreamCamera::YCbCrFrame>>();
    qRegisterMetaType<std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame>>();
    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/StreamCamera.qml")));
    view->show();

    return application->exec();
}
