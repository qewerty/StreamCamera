// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "mp4file.h"

Mp4File::Mp4File(unsigned int width, unsigned int height) : m_width(width), m_height(height)
{
    QDir resDir("Videos/StreamCamera/");
    if (!resDir.exists()) {
        QDir().mkdir(resDir.path());
    }
    QString filename = "Videos/StreamCamera/" + generateFileName() + ".mp4";
    avformat_alloc_output_context2(&m_avFormatContext, NULL, NULL, filename.toStdString().c_str());
    m_outputVideoStream = avformat_new_stream(m_avFormatContext, NULL);
    initOutputAVStream(m_outputVideoStream);
    int err = 0;
    if (!(m_avFormatContext->oformat->flags & AVFMT_NOFILE)) {
        if ((err = avio_open(&m_avFormatContext->pb, filename.toStdString().c_str(),
                             AVIO_FLAG_WRITE))
            < 0) {
            qDebug() << "Failed to open output file" << err;
            exit(-1);
        }
    }
    if ((err = avformat_write_header(m_avFormatContext, NULL)) < 0) {
        qDebug() << "Failed to write header to output file" << err;
        exit(-1);
    }
}

QString Mp4File::generateFileName()
{
    QDateTime date = QDateTime::currentDateTime();
    QString formattedTime = date.toString("yyyyMMdd_hhmmss");
    return formattedTime;
}

void Mp4File::saveAndClose()
{
    av_write_trailer(m_avFormatContext);
    avio_close(m_avFormatContext->pb);
    avformat_free_context(m_avFormatContext);
    qDebug() << "Video file is saved";
}

void Mp4File::initOutputAVStream(AVStream *outVideoStream)
{
    if (outVideoStream->codecpar == nullptr) {
        outVideoStream->codecpar = avcodec_parameters_alloc();
    }
    outVideoStream->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
    outVideoStream->codecpar->codec_id = AV_CODEC_ID_H264;
    outVideoStream->codecpar->width = m_width;
    outVideoStream->codecpar->height = m_height;
    outVideoStream->codecpar->format = AV_PIX_FMT_YUV420P;
    outVideoStream->codecpar->bit_rate = 2000000;
    outVideoStream->codecpar->profile = 0x01;
    outVideoStream->time_base = m_timeBase;
}

/**
 * Adds encoded H264 frame to the file. Expects a frame in the Annex-B format.
 */
void Mp4File::addEncodedFrame(EncodedFrame *frame, bool isKeyFrame)
{
    if (m_previousTimestampUs == 0) {
        m_duration = 0;
    } else {
        m_duration = frame->m_timestampUs - m_previousTimestampUs;
        m_pts += m_duration;
    }
    m_previousTimestampUs = frame->m_timestampUs;
    m_packet = new AVPacket();
    initAVPacket(m_packet);
    m_packet->data = frame->m_data;
    m_packet->size = frame->m_size;
    m_packet->stream_index = m_outputVideoStream->index;
    if (isKeyFrame)
        m_packet->flags = AV_PKT_FLAG_KEY;
    m_packet->pts = m_pts;
    m_packet->dts = m_packet->pts;
    av_write_frame(m_avFormatContext, m_packet);
    delete m_packet;
    m_packet = nullptr;
}

void Mp4File::initAVPacket(AVPacket *pkt)
{
    pkt->pts = AV_NOPTS_VALUE;
    pkt->dts = AV_NOPTS_VALUE;
    pkt->pos = -1;
    pkt->duration = 0;
    pkt->flags = 0;
    pkt->stream_index = 0;
    pkt->buf = NULL;
    pkt->side_data = NULL;
    pkt->side_data_elems = 0;
    pkt->data = NULL;
    pkt->size = 0;
}
