// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef MP4FILE_H
#define MP4FILE_H

#define NAL_SPS 0x67
#define NAL_PPS 0x68
#define NAL_I 0x65
#define NAL_P 0x41

#include "encodedframe.h"
#include <QDateTime>
#include <QDebug>
#include <QDir>

extern "C" {
#include "libavformat/avformat.h"
}

class Mp4File
{
public:
    Mp4File(unsigned int width, unsigned int height);
    void addEncodedFrame(EncodedFrame *frame, bool isKeyFrame);
    void saveAndClose();

private:
    AVRational m_timeBase = { 1, 1000000 };
    uint64_t m_previousTimestampUs = 0;
    uint64_t m_duration = 0;
    uint64_t m_pts = 0;
    AVFormatContext *m_avFormatContext = nullptr;
    AVPacket *m_packet = nullptr;
    AVStream *m_outputVideoStream = nullptr;
    unsigned int m_width;
    unsigned int m_height;
    QString generateFileName();
    void initAVPacket(AVPacket *pPacket);
    void initOutputAVStream(AVStream *outVideoStream);
};

#endif // MP4FILE_H
