// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YCBCRFRAMEMETATYPECONNECTOR_H
#define YCBCRFRAMEMETATYPECONNECTOR_H

#include <QMetaType>
#include <streamcamera/streamcamera.h>

Q_DECLARE_METATYPE(std::shared_ptr<Aurora::StreamCamera::YCbCrFrame>);
Q_DECLARE_METATYPE(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame>);

#endif // YCBCRFRAMEMETATYPECONNECTOR_H
