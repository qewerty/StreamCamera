// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "ycbcrtorgbimageconverter.h"

YCbCrToRgbImageConverter::YCbCrToRgbImageConverter(QObject *parent) : QObject(parent), QRunnable()
{
    setAutoDelete(true);
    m_runnning = true;
    m_isConverterFree = true;
}

void YCbCrToRgbImageConverter::onConvertNext(
        std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame)
{
    if (m_isConverterFree) {
        m_frame = frame;
        m_isConverterFree = false;
    }
}
void YCbCrToRgbImageConverter::run()
{
    while (m_runnning) {
        if (!m_isConverterFree) {
            if (m_frame) {
                QImage *image = convertToArgb(m_frame);
                emit imageConverted(image);
                m_frame = nullptr;
                m_isConverterFree = true;
            }
        }
    }
}

/**
 * Stops the main converting proccess loop.
 * After the loop will be stopped the object will be freed automaticaly.
 */
void YCbCrToRgbImageConverter::stop()
{
    m_runnning = false;
    m_frame = nullptr;
}

QImage *YCbCrToRgbImageConverter::convertToArgb(
        std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame)
{
    QSize size(frame->width, frame->height);
    QImage *image = new QImage(size, QImage::Format_ARGB32);
    planarYuv420ToArgb(frame->y, frame->cb, frame->cr, frame->yStride, frame->cStride,
                       frame->cStride, frame->chromaStep,
                       reinterpret_cast<quint32 *>(image->bits()), frame->width, frame->height);
    return image;
}

quint32 YCbCrToRgbImageConverter::yuvToArgb(qint32 y, qint32 rv, qint32 guv, qint32 bu,
                                            qint32 a = 255)
{
    qint32 yy = (y - 16) * 298;

    return (a << 24) | qBound(0, (yy + rv) >> 8, 255) << 16 | qBound(0, (yy - guv) >> 8, 255) << 8
            | qBound(0, (yy + bu) >> 8, 255);
}

void YCbCrToRgbImageConverter::planarYuv420ToArgb(const uchar *y, const uchar *u, const uchar *v,
                                                  qint32 yStride, qint32 uStride, qint32 vStride,
                                                  qint32 uvPixelStride, quint32 *rgb, qint32 width,
                                                  qint32 height)
{
    quint32 *rgb0 = rgb;
    quint32 *rgb1 = rgb + width;

    for (qint32 j = 0; j < height; j += 2) {
        const uchar *lineY0 = y;
        const uchar *lineY1 = y + yStride;
        const uchar *lineU = u;
        const uchar *lineV = v;

        for (qint32 i = 0; i < width; i += 2) {
            const qint32 uu = *lineU - 128;
            const qint32 vv = *lineV - 128;
            const qint32 rv = 409 * vv + 128;
            const qint32 guv = 100 * uu + 208 * vv + 128;
            const qint32 bu = 516 * uu + 128;

            lineU += uvPixelStride;
            lineV += uvPixelStride;
            *rgb0++ = yuvToArgb(*lineY0++, rv, guv, bu);
            *rgb0++ = yuvToArgb(*lineY0++, rv, guv, bu);
            *rgb1++ = yuvToArgb(*lineY1++, rv, guv, bu);
            *rgb1++ = yuvToArgb(*lineY1++, rv, guv, bu);
        }

        y += yStride << 1;
        u += uStride;
        v += vStride;
        rgb0 += width;
        rgb1 += width;
    }
}
