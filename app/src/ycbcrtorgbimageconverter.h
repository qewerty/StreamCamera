// SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef YCBCRTORGBIMAGECONVERTER_H
#define YCBCRTORGBIMAGECONVERTER_H

#include <QObject>
#include <QRunnable>
#include <QImage>
#include <QDebug>
#include <QSize>
#include <streamcamera/streamcamera.h>

class YCbCrToRgbImageConverter : public QObject, public QRunnable
{
    Q_OBJECT
public:
    explicit YCbCrToRgbImageConverter(QObject *parent = nullptr);
    void run();
    void stop();
signals:
    void imageConverted(QImage *image);
public slots:
    void onConvertNext(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame);

private:
    volatile bool m_runnning;
    bool m_isConverterFree;
    QImage *convertToArgb(std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> frame);
    std::shared_ptr<const Aurora::StreamCamera::YCbCrFrame> m_frame;
    quint32 yuvToArgb(qint32 y, qint32 rv, qint32 guv, qint32 bu, qint32 a);
    void planarYuv420ToArgb(const uchar *y, const uchar *u, const uchar *v, qint32 yStride,
                            qint32 uStride, qint32 vStride, qint32 uvPixelStride, quint32 *rgb,
                            qint32 width, qint32 height);
};

#endif // YCBCRTORGBIMAGECONVERTER_H
