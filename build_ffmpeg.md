# Building the ffmpeg library

The ffmpeg library is built on a building environment to work with the H264-codec and the MP4-container only.
If you need to use more capabilities of the ffmpeg you might rebuild the library using the next instruction appending with needed options. You must:

1. Download the latest linux-version [ffmpeg library](https://ffmpeg.org/download.html#build-linux).

2. Configure the building from the root dirictory ffmpeg with the command:
```
./configure \
--enable-shared \
--prefix=$HOME/ffmpeg_build \
--disable-static \
--disable-doc \
--disable-x86asm \
--disable-programs \
--disable-everything  \
--enable-muxer=mp4 \
--enable-muxer=h264 \
--enable-protocol=file \
--enable-pic
```

The `prefix` parameter is a path to save the building result.
All the specified parameters are collected to work with the H264-codec and the MP4-container only.


3. Call the commands
```
make -j
make install
```

4. The building result will be in the directory specified in `prefix`.
Next you must move the `lib` and `include` directories from the building result directory to
the `libs\ffmpeg` project directory.
