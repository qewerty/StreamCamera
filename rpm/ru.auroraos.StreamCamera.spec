Name:       ru.auroraos.StreamCamera

Summary:    The sample application thats uses capabilites of the StreamCamera library API.
Version:    0.1
Release:    1
Group:      Qt/Qt
License:    BSD-3-Clause
URL:        https://auroraos.ru
Source0:    %{name}-%{version}.tar.bz2

Requires:   sailfishsilica-qt5 >= 0.10.9
BuildRequires:  pkgconfig(auroraapp)
BuildRequires:  pkgconfig(Qt5Core)
BuildRequires:  pkgconfig(Qt5Qml)
BuildRequires:  pkgconfig(Qt5Quick)
BuildRequires:  pkgconfig(streamcamera)
BuildRequires:  pkgconfig(streamcamera-qt5)

%description
The sample application that uses capabilities of the StreamCamera library API.

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%define __provides_exclude_from ^%{_datadir}/.*$
%define __requires_exclude ^(libavcodec.*|libavdevice.*|libavfilter.*|libavformat.*|libavutil.*|libswresample.*|libswscale.*)$

%files
%defattr(-,root,root,-)
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
