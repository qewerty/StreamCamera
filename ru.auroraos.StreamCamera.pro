# SPDX-FileCopyrightText: 2023 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs

SUBDIRS = \
    app \

OTHER_FILES += \
    rpm/ru.auroraos.StreamCamera.spec \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-Clause.md \
    build_ffmpeg.md \
    README.md \
    README.ru.md
